﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ChillApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "NextStep",
                url: "next-step/{id}",
                defaults: new {controller = "Task", action = "NextStep", id = UrlParameter.Optional}
            );
            
            routes.MapRoute(
                name: "TaskEdit",
                url: "task-edit/{id}",
                defaults: new {controller = "Task", action = "Edit", id = UrlParameter.Optional}
            );
            
            routes.MapRoute(
                name: "TaskCreate",
                url: "new-task",
                defaults: new {controller = "Task", action = "Create"}
            );
            
            routes.MapRoute(
                name: "Logout",
                url: "log-out",
                defaults: new {controller = "Login", action = "Logout"}
            );
            
            routes.MapRoute(
                name: "Register",
                url: "register",
                defaults: new {controller = "Login", action = "Register"}
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new {controller = "Home", action = "Index", id = UrlParameter.Optional}
            );
        }
    }
}
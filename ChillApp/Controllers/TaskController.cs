using System;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ChillApp.Models.Context;
using ChillApp.Models.Dto;
using ChillApp.Models.Entites;

namespace ChillApp.Controllers
{
    public class TaskController : Controller
    {
        private DatabaseContext db = new DatabaseContext();
        public ActionResult Index()
        {
            var userLogin = Session["userLogin"] as User;
            TaskStatusModel taskStatusModel = new TaskStatusModel
            {
                Status = db.Status.Where(x=>x.StatusTypeId==2).ToList(),
                Task = db.Task.Where(x=>x.User.Id==userLogin.Id && x.IsDeleted==false).ToList()
            };
            return View(taskStatusModel);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Task model)
        {
            if (!string.IsNullOrEmpty(model.Title) && !string.IsNullOrEmpty(model.Descirption))
            {
                var userLogin = Session["userLogin"] as User;
                model.CreatedDate=DateTime.Now;
                model.UserId = userLogin.Id;
                model.StatusId = 3;
                db.Task.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }
        public ActionResult Edit(int? id)
        {
            if (id!=null)
            {
                Task result = db.Task.FirstOrDefault(x => x.Id == id);
                if (result==null)
                {
                    return HttpNotFound();
                }
                ViewBag.StatusId = new SelectList(db.Status.Where(x=>x.StatusTypeId==2).ToList(), "Id", "Name",result.StatusId);
                return View(result);
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        [HttpPost]
        public ActionResult Edit(Task model)
        {
            if (!string.IsNullOrEmpty(model.Title) && !string.IsNullOrEmpty(model.Descirption))
            {
                Task result = db.Task.FirstOrDefault(x => x.Id == model.Id);
                result.Title = model.Title;
                result.Descirption = model.Descirption;
                result.StatusId = model.StatusId;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.StatusId = new SelectList(db.Status.Where(x=>x.StatusTypeId==2).ToList(), "Id", "Name",model.StatusId);
            return View(model);
        }
        public ActionResult NextStep(int? id)
        {
            if (id!=null)
            {
                Task result = db.Task.FirstOrDefault(x => x.Id == id);
                if (result!=null)
                {
                    switch (result.StatusId)
                    {
                        case 3 : 
                            result.StatusId = 4;
                            break;
                        case 4 :
                            result.StatusId = 5; 
                            break;
                        case 5 :
                            result.IsDeleted = true;
                            break;
                    }

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
        // TODO delete actionu json olacak, bir page olmayacak. Swift Alert Kullanilabilir.
        public ActionResult Delete()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Delete(Task model)
        {
            return View();
        }
    }
}
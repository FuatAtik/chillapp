using System.Linq;
using System.Web.Mvc;
using ChillApp.Models.Context;
using ChillApp.Models.Entites;

namespace ChillApp.Controllers
{
    public class LoginController : Controller
    {
        private DatabaseContext db = new DatabaseContext();
        public ActionResult Index()
        {
            var userLogin = Session["userLogin"] as User;
            if (userLogin!=null)
                return RedirectToAction("Index", "Home");
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(User model)
        {
            var checkUser = db.User.FirstOrDefault(x => x.Username == model.Username && x.Password==model.Password);
            if (checkUser != null)
            {
                Session["userLogin"] = checkUser;
                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }
        
        public ActionResult Register()
        {
            var userLogin = Session["userLogin"] as User;
            if (userLogin!=null)
                return RedirectToAction("Index", "Home");
            
            return View();
        }
        
        [HttpPost]
        public ActionResult Register(User model)
        {
            var checkUser = db.User.FirstOrDefault(x => x.Username == model.Username);
            if (checkUser==null)
            {
                db.User.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UsernameError = "This username already use";
            return View(model);
        }
        
        public ActionResult Logout()
        {
            var userLogin = Session["userLogin"] as User;
            if (userLogin != null)
            {
                Session.Clear();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
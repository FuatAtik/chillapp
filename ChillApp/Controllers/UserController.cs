using System.Linq;
using System.Web.Mvc;
using ChillApp.Models.Context;
using ChillApp.Models.Dto;
using ChillApp.Models.Entites;

namespace ChillApp.Controllers
{
    public class UserController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET
        public ActionResult Profile()
        {
            var userLogin = Session["userLogin"] as User;
            TaskStatusModel taskStatusModel = new TaskStatusModel
            {
                Status = db.Status.Where(x=>x.StatusTypeId==2).ToList(),
                Task = db.Task.Where(x=>x.User.Id==userLogin.Id && x.IsDeleted).ToList()
            };
            return View(taskStatusModel);
        }
        
        public ActionResult DeletedTasks()
        {
            
            return View();
        }
    }
}
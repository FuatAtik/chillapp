﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChillApp.Models.Entites;

namespace ChillApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var userLogin = Session["userLogin"] as User;
            if (userLogin!=null)
                return View();

            return RedirectToAction("Index", "Login");
        }
    }
}
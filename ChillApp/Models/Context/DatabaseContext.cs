using System.Data.Entity;
using ChillApp.Models.Entites;

namespace ChillApp.Models.Context
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext()
            : base("DatabaseContext")
        {
        }
        public DbSet<User> User { get; set; }
        public DbSet<Task> Task { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<StatusType> StatusType { get; set; }
    }
}
using System;

namespace ChillApp.Models.Entites
{
    public class Task
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Descirption { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool IsDeleted { get; set; }

        public int StatusId { get; set; }
        public virtual Status Status { get; set; }
        
        public int UserId { get; set; }
        public virtual User User { get; set; }      

        
    }
}
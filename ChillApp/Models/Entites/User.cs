using System.Collections.Generic;

namespace ChillApp.Models.Entites
{
    public class User
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }

        public ICollection<Task> Tasks { get; set; }
    }
}
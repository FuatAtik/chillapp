using System.Collections.Generic;

namespace ChillApp.Models.Entites
{
    public class StatusType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
        
        public ICollection<Status> Status{ get; set; }
    }
}
using System.Collections.Generic;

namespace ChillApp.Models.Entites
{
    public class Status
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int StatusTypeId { get; set; }
        public virtual StatusType StatusType { get; set; }
        
        public ICollection<Task> Tasks { get; set; }
        public ICollection<User> Users { get; set; }

    }
}
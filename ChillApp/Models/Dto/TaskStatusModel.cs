using System.Collections.Generic;
using ChillApp.Models.Entites;

namespace ChillApp.Models.Dto
{
    public class TaskStatusModel
    {
        public List<Task> Task { get; set; }
        public List<Status> Status { get; set; }
    }
}